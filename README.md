# README

_Technical Interview: Ruby Test._
A mini test using the RecipePuppy API

## Project
A recipe finder app using the RecipePuppy API:  http://www.recipepuppy.com/about/api/

**Version:** 1.0

### Requirements
  - The user types in a search query for the name of a recipe and hits enter (submit)
  - Only the first 20 results of a search are displayed
  - Code should be written in Rails 5.0.3 & Ruby 2.4.0
  - Frontend should use Bootstrap 4 for styling

### Assessment criteria
1. How well the requirements above are met
2. Code quality
3. The simplicity and extensibility of the approach taken

### Help
Please email if you have any questions about the requirements.
We hope you enjoy the technical test and look forward to analysing your code!

## How do I get set up?

### Summary of set up
1. Clone the repository: `git clone git@bitbucket.org:misc-code/recipe_puppy_api.git`
1. Install dependencies: `bundle install`
1. Create the database: `rake db:create`
1. Migrate the database: `rake db:migrate`
1. Run the application: `puma` and open the URL: `http://localhost:3000/`

### Configuration
#### Dependencies
- Ruby 2.4.0 (Install Ruby through [Ruby Version Manager](https://rvm.io/rvm/install#1-download-and-run-the-rvm-installation-script), [Instal Specific version of ruby](https://rvm.io/rvm/install#try-out-your-new-rvm-installation))
- Bundler: `gem install bundler`
- Rails 5.0.3: `gem install rails -v 5.0.3`
- PostgreSQL
- Install project dependencies: `bundle install`

#### How to run tests
- Run all Unitary Tests (TDD): `rspec spec`
- Run Automation Tests (ATDD) [silent mode]: `rspec spec/features`
    - **Note:** In order to run Autamated Test in the browser use the following command:
    - Chrome: `BROWSER=chrome rspec spec/features`
    - FireFox: `BROWSER=ff rspec spec/features`

#### Deployment instructions
- Heroku Server: `git push heroku master`
- Production App URL: https://freedie-test.herokuapp.com/

### Who do I talk to? ###

**Dennys López Dinza**
_Agile/SCRUM Full Stack Software Developer_
dennys.infor@gmail.com

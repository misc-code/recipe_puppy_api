require 'rails_helper'

feature "Application", js: true do
  describe "Layout" do
    background do
      visit '/'
    end

    scenario "Show title as a link" do
      nav = find('nav.navbar')
      title_link = nav.find('a.navbar-brand')

      expect(title_link.text).to eq 'Recipe finder'
      expect(title_link[:href]).to end_with '/search'
    end

    scenario "Toggler navigation bar is exists but hidden" do
      nav = find('nav.navbar')
      nav.assert_selector('button.navbar-toggler', visible: false)
    end

    scenario "Show Menu of links" do
      nav = find('nav.navbar')
      menu = nav.find('ul.navbar-nav')
      items = menu.all('li')

      item = items.first
      link = item.find('a.nav-link')
      expect(link.text).to start_with 'Home'
      expect(link[:href]).to end_with '/'
    end
  end
end

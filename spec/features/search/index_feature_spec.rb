require 'rails_helper'
require 'cgi'

feature 'Search', js: true do
  background { visit '/' }

  describe "Search recipes" do
    scenario "Show an input to search by name" do
      input = find('input.form-control[type=text]')

      expect(input[:placeholder]).to eq 'Search for...'
      expect(input[:name]).to eq 'q'
    end

    scenario "Show a button to search" do
      button = find('div.input-group input[type=submit]')

      expect(button[:value]).to eq 'Search'
    end

    scenario "Show notification when no recipes" do
      page.assert_selector('div.empty .alert', text: 'No recipes in the list')
      page.assert_no_selector('table#results')
    end

    describe "List of recipes" do
      let!(:q) { 'omelet' }
      let(:items) {[
        Fabricate(:recipe, title: 'Baked Omelet With Broccoli &amp; Tomato', href: "http://www.recipezaar.com/Baked-Omelet-With-Broccoli-Tomato-325014", ingredients: ["milk", "cottage cheese", "broccoli", "cheddar cheese", "basil", "onion powder", "eggs", "garlic powder", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/123889.jpg"),
        Fabricate(:recipe, title: "Mild Curry Omelet", href: "http://allrecipes.com/Recipe/Mild-Curry-Omelet/Detail.aspx", ingredients: ["coriander", "cumin", "eggs", "garlic", "green onion", "vegetable oil", "onions", "red pepper", "salt", "turmeric"], thumbnail: ''),
        Fabricate(:recipe, title: "Light Italian Feta Omelet", href: "http://www.recipezaar.com/Light-Italian-Feta-Omelet-281901", ingredients: ["egg substitute", "feta cheese", "basil", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/36027.jpg")
      ]}

      background do
        lists = []
        (20 / items.size + 1).times { lists.concat items }
        allow_any_instance_of(SearchController).to receive(:retrieve_items).and_return lists

        fill_in 'q', with: q
        click_button 'Search'
      end

      scenario "Show a table of results" do
        page.assert_selector('table#results')
        page.assert_no_selector('div.empty .alert')
      end

      scenario "Show the number of items in the list" do
        page.assert_selector('div.summary', text: 'Total recipes in the list: 20')
      end

      scenario "Show the recipe information" do
        rows = all('table#results tbody tr')
        default_image = 'http://img.recipepuppy.com/9.jpg'

        expect(rows.size).to eq 20

        expect(rows[0].find('.information h6').text).to eq CGI.unescapeHTML(items[0].title)
        expect(rows[0].find('.information h6 a')[:href]).to eq items[0].href
        expect(rows[0].find('.information span.ingredients').text).to eq items[0].ingredients.join(' ')
        expect(rows[0].find('.thumbnail img')[:src]).to eq items[0].thumbnail

        expect(rows[1].find('.information h6').text).to eq CGI.unescapeHTML(items[1].title)
        expect(rows[1].find('.information h6 a')[:href]).to eq items[1].href
        expect(rows[1].find('.information span.ingredients').text).to eq items[1].ingredients.join(' ')
        expect(rows[1].find('.thumbnail img')[:src]).to eq default_image
      end
    end
  end

  describe "Failures" do
    context "API failed" do
      background do
        allow_any_instance_of(SearchController).to receive(:retrieve_items).and_raise "API failed"

        fill_in 'q', with: 'test'
        click_button 'Search'
      end

      scenario "Show alert danger messages" do
        error_messages = all('div.notifications .alert-danger p')
        error_messages = error_messages.map { |error_line| error_line.text }

        expect(error_messages).to include "API failed"
      end
    end
  end
end

require 'capybara/poltergeist'

case ENV['BROWSER']
when 'chrome'
  # Google Chrome
  Capybara.register_driver :selenium do |app|
    options = {
      :js_errors => false,
      :timeout => 360,
      :debug => false,
      :inspector => false,
    }
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

when 'ff'
  # Mozilla Firefox
  Capybara.register_driver :selenium do |app|
    options = {
      :js_errors => false,
      :timeout => 360,
      :debug => false,
      :inspector => false,
    }
    Capybara::Selenium::Driver.new(app, :browser => :firefox)
  end
else
  # Poltergeist
  Capybara.register_driver :selenium do |app|
    options = {
      :js_errors => false,
      :timeout => 360,
      :debug => false,
      :phantomjs_options => ['--load-images=no', '--disk-cache=false'],
      :inspector => false,
    }
    Capybara::Poltergeist::Driver.new(app, options)
  end
end

Capybara.javascript_driver = :selenium

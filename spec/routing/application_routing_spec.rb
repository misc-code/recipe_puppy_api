require 'rails_helper'

describe 'Application' do
  it "routes to default root" do
    expect(get '/').to route_to 'search#index'
  end
end

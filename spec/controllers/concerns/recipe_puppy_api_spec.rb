require 'rails_helper'

describe 'Recipe Puppy API' do
  let!(:q) { 'omelet' }
  let(:items) {[
    Fabricate(:recipe, title: 'Baked Omelet With Broccoli &amp; Tomato', href: "http://www.recipezaar.com/Baked-Omelet-With-Broccoli-Tomato-325014", ingredients: ["milk", "cottage cheese", "broccoli", "cheddar cheese", "basil", "onion powder", "eggs", "garlic powder", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/123889.jpg"),
    Fabricate(:recipe, title: "Mild Curry Omelet", href: "http://allrecipes.com/Recipe/Mild-Curry-Omelet/Detail.aspx", ingredients: ["coriander", "cumin", "eggs", "garlic", "green onion", "vegetable oil", "onions", "red pepper", "salt", "turmeric"], thumbnail: ''),
    Fabricate(:recipe, title: "Light Italian Feta Omelet", href: "http://www.recipezaar.com/Light-Italian-Feta-Omelet-281901", ingredients: ["egg substitute", "feta cheese", "basil", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/36027.jpg")
  ]}

  it "retrieves recipes" do
    allow_any_instance_of(RecipePuppyAPI).to receive(:request).and_return File.read(Rails.root.join('spec/data/recipe_puppy_api_response.json'))
    puppy_api = RecipePuppyAPI.new
    recipes = puppy_api.retrieve_recipes(q: q)

    expect(recipes.to_json).to eq items.to_json
  end

  it "converts a response item into a object" do
    allow_any_instance_of(RecipePuppyAPI).to receive(:request).and_return File.read(Rails.root.join('spec/data/recipe_puppy_api_response.json'))
    puppy_api = RecipePuppyAPI.new

    recipes = puppy_api.retrieve_recipes(q: q)

    expect(recipes.first.class).to eq Recipe
  end
end

require 'rails_helper'

describe SearchController, type: :controller do
  describe "get #index" do
    context "without processing search" do
      before { get :index, params: {}, format: :html }

      it "assigns the subtitle" do
        expect(assigns(:titles).last).to eq 'Search'
      end

      it "should has no items" do
        expect(assigns(:items)).to be_empty
      end
    end

    context "with items found" do
      let!(:q) { 'omelet' }
      let(:items) {[
        Fabricate(:recipe, title: 'Baked Omelet With Broccoli &amp; Tomato', href: "http://www.recipezaar.com/Baked-Omelet-With-Broccoli-Tomato-325014", ingredients: ["milk", "cottage cheese", "broccoli", "cheddar cheese", "basil", "onion powder", "eggs", "garlic powder", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/123889.jpg"),
        Fabricate(:recipe, title: "Mild Curry Omelet", href: "http://allrecipes.com/Recipe/Mild-Curry-Omelet/Detail.aspx", ingredients: ["coriander", "cumin", "eggs", "garlic", "green onion", "vegetable oil", "onions", "red pepper", "salt", "turmeric"], thumbnail: ''),
        Fabricate(:recipe, title: "Light Italian Feta Omelet", href: "http://www.recipezaar.com/Light-Italian-Feta-Omelet-281901", ingredients: ["egg substitute", "feta cheese", "basil", "roma tomato", "salt"], thumbnail: "http://img.recipepuppy.com/36027.jpg")
      ]}

      it "assigns the data found" do
        allow_any_instance_of(RecipePuppyAPI).to receive(:request).and_return File.read(Rails.root.join('spec/data/recipe_puppy_api_response.json'))

        get :index, params: { commit: "Search", q: q }

        expect(assigns(:items)).to_not be_empty
        expect(assigns(:items).to_json).to eq items.to_json
      end

      it "has up to 20 items" do
        lists = []
        (20 / items.size + 1).times { lists.concat items }

        allow_any_instance_of(SearchController).to receive(:retrieve_items).and_return lists
        get :index, params: { commit: "Search", q: q }

        expect(assigns(:items).count).to eq 20
      end
    end

    context "no found" do
      it "has items empty" do
        allow_any_instance_of(RecipePuppyAPI).to receive(:request).and_return File.read(Rails.root.join('spec/data/recipe_puppy_api_empty_response.json'))

        get :index, params: { commit: "Search", q: 'wrong_name' }

        expect(assigns(:items)).to be_empty
      end
    end

    context "with API failure" do
      it "assigns the error to a flash message" do
        allow_any_instance_of(RecipePuppyAPI).to receive(:request).and_raise "API Server error. Try again later."
        get :index, params: { commit: "Search", q: 'name' }

        expect(flash[:error]).to include "API Server error. Try again later."
      end
    end
  end
end

require 'rails_helper';

describe Recipe, type: :model do
  subject { Recipe.new }

  it "creats a Recipe" do
    expect(subject).to be_a Recipe
    expect(subject.title).to eq ''
    expect(subject.href).to eq ''
    expect(subject.ingredients).to eq []
    expect(subject.thumbnail).to eq ''
  end
end

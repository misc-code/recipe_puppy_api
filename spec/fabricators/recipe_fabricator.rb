Fabricator :recipe do
  title       'name 1'
  href        'http://recipe.test/name-1'
  ingredients [ 'milk', 'salt' ]
  thumbnail   'http://recipe.test/name-1.png'
end

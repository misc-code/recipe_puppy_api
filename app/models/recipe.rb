class Recipe
  attr_accessor :title, :href, :ingredients, :thumbnail

  def initialize
    @title = ''
    @href = ''
    @ingredients = []
    @thumbnail = ''
  end
end

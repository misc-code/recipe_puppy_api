require "net/http"
require "uri"

class RecipePuppyAPI
  def initialize
    @host = 'http://www.recipepuppy.com/'
    @api_base = 'api/'
    @q = ''

    @uri = URI.parse "#{@host}#{@api_base}"
  end

  def retrieve_recipes params
    map_items(JSON.parse request(params))
  end

  def to_recipe item
    recipe = Recipe.new
    recipe.title = item['title']
    recipe.href = item['href']
    recipe.ingredients = item['ingredients'].split(', ')
    recipe.thumbnail = item['thumbnail']

    recipe
  end

  private

  def request params
    @uri.query = URI.encode_www_form params

    http = Net::HTTP.new(@uri.host, @uri.port)

    request = Net::HTTP::Get.new(@uri.request_uri)

    response = http.request request

    raise "API Server error. Try again later." unless response.code == '200'

    response.body
  end

  def map_items response_body
    response_body["results"].map { |item| to_recipe item }
  end
end

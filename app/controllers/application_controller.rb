class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :application_title, :initialize_flash_messages

  private

  def initialize_flash_messages
    flash[:error] = []
    flash[:success] = []
  end

  def application_title
    @titles = ['Recipe Finder']
  end

  def is_production?
    Rails.env == 'production'
  end
end

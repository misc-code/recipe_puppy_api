require 'will_paginate/array'

class SearchController < ApplicationController
  http_basic_authenticate_with name: RecipePuppyApi::Application.config.basic_auth_user, password: RecipePuppyApi::Application.config.basic_auth_password, if: :is_production?

  def index
    @titles.push 'Search'
    @items = []

    begin
      if params[:commit].present?
        @items = retrieve_items
        @items = @items.paginate(page: 1, per_page: RecipePuppyApi::Application.config.items_per_page) if @items.size > RecipePuppyApi::Application.config.items_per_page
      end
    rescue Exception => e
      flash[:error].push e.message
    end
  end

  private

  def retrieve_items
    puppy_api = RecipePuppyAPI.new
    puppy_api.retrieve_recipes(q: params[:q])
  end
end
